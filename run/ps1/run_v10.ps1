conda activate mpipyWin

cd run/mpi

echo ' CREATING PROJECT DIRECTORY '                       'v10'

$FileName=                              "..\..\simulations\v10"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\simulations\v00.tar.gz -C ..\..\simulations
mv       ..\..\simulations\v00           ..\..\simulations\v10

echo ' RUNNING A TYPICAL SIMULATION '
mpiexec -np 8 python solver.py -v v10 -c 1

echo ' AGGREGATING PROCESSOR DATA '
python aggregator.py          -v v10

echo ' RUNNING VISUALIZATIONS '
matlab -batch "graphics(0,'v10',{0.1})" -noFigureWindows

cd ..\..\

conda deactivate mpipyWin

