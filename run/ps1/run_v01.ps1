conda activate pyWin

cd run/mpi

echo ' CREATING PROJECT DIRECTORY '                       'v01'

$FileName=                              "..\..\simulations\v01"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xzf ..\..\simulations\v00.tar.gz -C ..\..\simulations
mv       ..\..\simulations\v00           ..\..\simulations\v01

echo ' RUNNING A TYPICAL SIMULATION '
python solverSER.py -v v01 -c 1

echo ' RUNNING VISUALIZATIONS '
matlab -batch "graphics(0,'v01',{0.1})" -noFigureWindows

cd ..\..\

conda deactivate pyWin

