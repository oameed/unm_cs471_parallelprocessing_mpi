#! /bin/bash

source  activate mpipy
cd      run/mpi

echo ' CREATING PROJECT DIRECTORY '                            'v10'
rm     -rf                                    ../../simulations/v10
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v10
echo ' RUNNING A TYPICAL SIMULATION '
mpirun -np 8 python solver.py -v v10 -c 1
echo ' AGGREGATING PROCESSOR DATA '
python aggregator.py          -v v10
echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics(0,'v10',{0.1});exit;"

