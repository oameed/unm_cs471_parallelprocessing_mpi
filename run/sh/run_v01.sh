#! /bin/bash


cd      run/mpi

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                    ../../simulations/v01
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v01
echo ' RUNNING A TYPICAL SIMULATION '
python solverSER.py -v v01 -c 1


echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics(0,'v01',{0.1});exit;"

