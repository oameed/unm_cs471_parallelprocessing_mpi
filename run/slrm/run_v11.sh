#!/bin/bash

#SBATCH --ntasks=8
#SBATCH --time=1:00:00
#SBATCH --partition=normal
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=parlprj

module load miniconda3
module load matlab
source activate mpipy

cd $SLURM_SUBMIT_DIR
cd ../mpi

echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                    ../../simulations/v11
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v11

echo ' RUNNING A TYPICAL SIMULATION '
mpiexec -n 8 python solver.py -v v11 -c 1

echo ' AGGREGATING PROCESSOR DATA '
python aggregator.py          -v v11

echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics(0,'v11',{0.1});exit;"


