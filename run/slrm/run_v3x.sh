#!/bin/bash

#SBATCH --ntasks=16
#SBATCH --time=48:00:00
#SBATCH --partition=normal
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=parlprj

module load miniconda3
module load matlab
source activate mpipy

cd $SLURM_SUBMIT_DIR
cd ../mpi

echo ' RUNNING WEAK SCALING TESTS '

echo ' CREATING PROJECT DIRECTORIES '

rm     -rf                                    ../../simulations/v30
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v30

rm     -rf                                    ../../simulations/v31
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v31

rm     -rf                                    ../../simulations/v32
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v32

rm     -rf                                    ../../simulations/v33
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v33

rm     -rf                                    ../../simulations/v34
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v34

#############
### RUN 1 ###
#############
mkdir -p ../../simulations/v30/runs/run1

echo ' RUN: 1, 1  PROCESSORS IN v31'
mpiexec -n 1   python solver.py -v v31 -c 3 -t 0.03 -noexp
mv ../../simulations/v31/*.csv ../../simulations/v30/runs/run1

echo ' RUN: 1, 4  PROCESSORS IN v32'
mpiexec -n 4   python solver.py -v v32 -c 4 -t 0.03 -noexp
mv ../../simulations/v32/*.csv ../../simulations/v30/runs/run1

echo ' RUN: 1, 10 PROCESSORS IN v33'
mpiexec -n 10  python solver.py -v v33 -c 5 -t 0.03 -noexp
mv ../../simulations/v33/*.csv ../../simulations/v30/runs/run1

echo ' RUN: 1, 16 PROCESSORS IN v34'
mpiexec -n 16  python solver.py -v v34 -c 6 -t 0.03 -noexp
mv ../../simulations/v34/*.csv ../../simulations/v30/runs/run1

#############
### RUN 2 ###
#############
mkdir -p ../../simulations/v30/runs/run2

echo ' RUN: 2, 1  PROCESSORS IN v31'
mpiexec -n 1   python solver.py -v v31 -c 3 -t 0.03 -noexp
mv ../../simulations/v31/*.csv ../../simulations/v30/runs/run2

echo ' RUN: 2, 4  PROCESSORS IN v32'
mpiexec -n 4   python solver.py -v v32 -c 4 -t 0.03 -noexp
mv ../../simulations/v32/*.csv ../../simulations/v30/runs/run2

echo ' RUN: 2, 10 PROCESSORS IN v33'
mpiexec -n 10  python solver.py -v v33 -c 5 -t 0.03 -noexp
mv ../../simulations/v33/*.csv ../../simulations/v30/runs/run2

echo ' RUN: 2, 16 PROCESSORS IN v34'
mpiexec -n 16  python solver.py -v v34 -c 6 -t 0.03 -noexp
mv ../../simulations/v34/*.csv ../../simulations/v30/runs/run2

#############
### RUN 3 ###
#############
mkdir -p ../../simulations/v30/runs/run3

echo ' RUN: 3, 1  PROCESSORS IN v31'
mpiexec -n 1   python solver.py -v v31 -c 3 -t 0.03 -noexp
mv ../../simulations/v31/*.csv ../../simulations/v30/runs/run3

echo ' RUN: 3, 4  PROCESSORS IN v32'
mpiexec -n 4   python solver.py -v v32 -c 4 -t 0.03 -noexp
mv ../../simulations/v32/*.csv ../../simulations/v30/runs/run3

echo ' RUN: 3, 10 PROCESSORS IN v33'
mpiexec -n 10  python solver.py -v v33 -c 5 -t 0.03 -noexp
mv ../../simulations/v33/*.csv ../../simulations/v30/runs/run3

echo ' RUN: 3, 16 PROCESSORS IN v34'
mpiexec -n 16  python solver.py -v v34 -c 6 -t 0.03 -noexp
mv ../../simulations/v34/*.csv ../../simulations/v30/runs/run3

#############
### RUN 4 ###
#############
mkdir -p ../../simulations/v30/runs/run4

echo ' RUN: 4, 1  PROCESSORS IN v31'
mpiexec -n 1   python solver.py -v v31 -c 3 -t 0.03 -noexp
mv ../../simulations/v31/*.csv ../../simulations/v30/runs/run4

echo ' RUN: 4, 4  PROCESSORS IN v32'
mpiexec -n 4   python solver.py -v v32 -c 4 -t 0.03 -noexp
mv ../../simulations/v32/*.csv ../../simulations/v30/runs/run4

echo ' RUN: 4, 10 PROCESSORS IN v33'
mpiexec -n 10  python solver.py -v v33 -c 5 -t 0.03 -noexp
mv ../../simulations/v33/*.csv ../../simulations/v30/runs/run4

echo ' RUN: 4, 16 PROCESSORS IN v34'
mpiexec -n 16  python solver.py -v v34 -c 6 -t 0.03 -noexp
mv ../../simulations/v34/*.csv ../../simulations/v30/runs/run4

#############
### RUN 5 ###
#############
mkdir -p ../../simulations/v30/runs/run5

echo ' RUN: 5, 1  PROCESSORS IN v31'
mpiexec -n 1   python solver.py -v v31 -c 3 -t 0.03 -noexp
mv ../../simulations/v31/*.csv ../../simulations/v30/runs/run5

echo ' RUN: 5, 4  PROCESSORS IN v32'
mpiexec -n 4   python solver.py -v v32 -c 4 -t 0.03 -noexp
mv ../../simulations/v32/*.csv ../../simulations/v30/runs/run5

echo ' RUN: 5, 10 PROCESSORS IN v33'
mpiexec -n 10  python solver.py -v v33 -c 5 -t 0.03 -noexp
mv ../../simulations/v33/*.csv ../../simulations/v30/runs/run5

echo ' RUN: 5, 16 PROCESSORS IN v34'
mpiexec -n 16  python solver.py -v v34 -c 6 -t 0.03 -noexp
mv ../../simulations/v34/*.csv ../../simulations/v30/runs/run5

###

echo ' AGGREGATING TIME MEASUREMENTS '
python aggregator.py -v v30

echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics(2,{'v30'},{});exit;"

