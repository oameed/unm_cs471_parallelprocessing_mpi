#!/bin/bash

#SBATCH --ntasks=128
#SBATCH --time=48:00:00
#SBATCH --partition=normal
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=parlprj

module load miniconda3
module load matlab
source activate mpipy

cd $SLURM_SUBMIT_DIR
cd ../mpi

echo ' RUNNING STRONG SCALING TESTS '

echo ' CREATING PROJECT DIRECTORIES '

rm     -rf                                    ../../simulations/v20
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v20

rm     -rf                                    ../../simulations/v22
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v22

rm     -rf                                    ../../simulations/v23
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v23

rm     -rf                                    ../../simulations/v24
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v24

rm     -rf                                    ../../simulations/v25
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v25

rm     -rf                                    ../../simulations/v26
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v26

#############
### RUN 1 ###
#############
mkdir -p ../../simulations/v20/runs/run1

echo ' RUN: 1, FIXED PROBLEM SIZE WITH 8   PROCESSORS IN v22'
mpiexec -n 8   python solver.py -v v22 -c 2 -t 0.015 -noexp
mv ../../simulations/v22/*.csv ../../simulations/v20/runs/run1

echo ' RUN: 1, FIXED PROBLEM SIZE WITH 16  PROCESSORS IN v23'
mpiexec -n 16  python solver.py -v v23 -c 2 -t 0.015 -noexp
mv ../../simulations/v23/*.csv ../../simulations/v20/runs/run1

echo ' RUN: 1, FIXED PROBLEM SIZE WITH 32  PROCESSORS IN v24'
mpiexec -n 32  python solver.py -v v24 -c 2 -t 0.015 -noexp
mv ../../simulations/v24/*.csv ../../simulations/v20/runs/run1

echo ' RUN: 1, FIXED PROBLEM SIZE WITH 64  PROCESSORS IN v25'
mpiexec -n 64  python solver.py -v v25 -c 2 -t 0.015 -noexp
mv ../../simulations/v25/*.csv ../../simulations/v20/runs/run1

echo ' RUN: 1, FIXED PROBLEM SIZE WITH 128 PROCESSORS IN v26'
mpiexec -n 128 python solver.py -v v26 -c 2 -t 0.015 -noexp
mv ../../simulations/v26/*.csv ../../simulations/v20/runs/run1

#############
### RUN 2 ###
#############
mkdir -p ../../simulations/v20/runs/run2

echo ' RUN: 2, FIXED PROBLEM SIZE WITH 8   PROCESSORS IN v22'
mpiexec -n 8   python solver.py -v v22 -c 2 -t 0.015 -noexp
mv ../../simulations/v22/*.csv ../../simulations/v20/runs/run2

echo ' RUN: 2, FIXED PROBLEM SIZE WITH 16  PROCESSORS IN v23'
mpiexec -n 16  python solver.py -v v23 -c 2 -t 0.015 -noexp
mv ../../simulations/v23/*.csv ../../simulations/v20/runs/run2

echo ' RUN: 2, FIXED PROBLEM SIZE WITH 32  PROCESSORS IN v24'
mpiexec -n 32  python solver.py -v v24 -c 2 -t 0.015 -noexp
mv ../../simulations/v24/*.csv ../../simulations/v20/runs/run2

echo ' RUN: 2, FIXED PROBLEM SIZE WITH 64  PROCESSORS IN v25'
mpiexec -n 64  python solver.py -v v25 -c 2 -t 0.015 -noexp
mv ../../simulations/v25/*.csv ../../simulations/v20/runs/run2

echo ' RUN: 2, FIXED PROBLEM SIZE WITH 128 PROCESSORS IN v26'
mpiexec -n 128 python solver.py -v v26 -c 2 -t 0.015 -noexp
mv ../../simulations/v26/*.csv ../../simulations/v20/runs/run2

#############
### RUN 3 ###
#############
mkdir -p ../../simulations/v20/runs/run3

echo ' RUN: 3, FIXED PROBLEM SIZE WITH 8   PROCESSORS IN v22'
mpiexec -n 8   python solver.py -v v22 -c 2 -t 0.015 -noexp
mv ../../simulations/v22/*.csv ../../simulations/v20/runs/run3

echo ' RUN: 3, FIXED PROBLEM SIZE WITH 16  PROCESSORS IN v23'
mpiexec -n 16  python solver.py -v v23 -c 2 -t 0.015 -noexp
mv ../../simulations/v23/*.csv ../../simulations/v20/runs/run3

echo ' RUN: 3, FIXED PROBLEM SIZE WITH 32  PROCESSORS IN v24'
mpiexec -n 32  python solver.py -v v24 -c 2 -t 0.015 -noexp
mv ../../simulations/v24/*.csv ../../simulations/v20/runs/run3

echo ' RUN: 3, FIXED PROBLEM SIZE WITH 64  PROCESSORS IN v25'
mpiexec -n 64  python solver.py -v v25 -c 2 -t 0.015 -noexp
mv ../../simulations/v25/*.csv ../../simulations/v20/runs/run3

echo ' RUN: 3, FIXED PROBLEM SIZE WITH 128 PROCESSORS IN v26'
mpiexec -n 128 python solver.py -v v26 -c 2 -t 0.015 -noexp
mv ../../simulations/v26/*.csv ../../simulations/v20/runs/run3

#############
### RUN 4 ###
#############
mkdir -p ../../simulations/v20/runs/run4

echo ' RUN: 4, FIXED PROBLEM SIZE WITH 8   PROCESSORS IN v22'
mpiexec -n 8   python solver.py -v v22 -c 2 -t 0.015 -noexp
mv ../../simulations/v22/*.csv ../../simulations/v20/runs/run4

echo ' RUN: 4, FIXED PROBLEM SIZE WITH 16  PROCESSORS IN v23'
mpiexec -n 16  python solver.py -v v23 -c 2 -t 0.015 -noexp
mv ../../simulations/v23/*.csv ../../simulations/v20/runs/run4

echo ' RUN: 4, FIXED PROBLEM SIZE WITH 32  PROCESSORS IN v24'
mpiexec -n 32  python solver.py -v v24 -c 2 -t 0.015 -noexp
mv ../../simulations/v24/*.csv ../../simulations/v20/runs/run4

echo ' RUN: 4, FIXED PROBLEM SIZE WITH 64  PROCESSORS IN v25'
mpiexec -n 64  python solver.py -v v25 -c 2 -t 0.015 -noexp
mv ../../simulations/v25/*.csv ../../simulations/v20/runs/run4

echo ' RUN: 4, FIXED PROBLEM SIZE WITH 128 PROCESSORS IN v26'
mpiexec -n 128 python solver.py -v v26 -c 2 -t 0.015 -noexp
mv ../../simulations/v26/*.csv ../../simulations/v20/runs/run4

#############
### RUN 5 ###
#############
mkdir -p ../../simulations/v20/runs/run5

echo ' RUN: 5, FIXED PROBLEM SIZE WITH 8   PROCESSORS IN v22'
mpiexec -n 8   python solver.py -v v22 -c 2 -t 0.015 -noexp
mv ../../simulations/v22/*.csv ../../simulations/v20/runs/run5

echo ' RUN: 5, FIXED PROBLEM SIZE WITH 16  PROCESSORS IN v23'
mpiexec -n 16  python solver.py -v v23 -c 2 -t 0.015 -noexp
mv ../../simulations/v23/*.csv ../../simulations/v20/runs/run5

echo ' RUN: 5, FIXED PROBLEM SIZE WITH 32  PROCESSORS IN v24'
mpiexec -n 32  python solver.py -v v24 -c 2 -t 0.015 -noexp
mv ../../simulations/v24/*.csv ../../simulations/v20/runs/run5

echo ' RUN: 5, FIXED PROBLEM SIZE WITH 64  PROCESSORS IN v25'
mpiexec -n 64  python solver.py -v v25 -c 2 -t 0.015 -noexp
mv ../../simulations/v25/*.csv ../../simulations/v20/runs/run5

echo ' RUN: 5, FIXED PROBLEM SIZE WITH 128 PROCESSORS IN v26'
mpiexec -n 128 python solver.py -v v26 -c 2 -t 0.015 -noexp
mv ../../simulations/v26/*.csv ../../simulations/v20/runs/run5

###

echo ' AGGREGATING TIME MEASUREMENTS '
python aggregator.py -v v20

echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics(1,{'v20'},{});exit;"

