#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### DATA AGGREGATOR FOR PARALLEL OPERATION                            ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import                                os
import                                sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import               numpy         as np
from paramd import   PATHS, PARAMS
from utilsd import  (getFILENAMES,
                     rHDF        ,
                     wHDF         )

if  PARAMS[0][0] in ['v20','v30']:
 pth_runs  =os.path.join(PATHS[0],'runs')
 num_runs  =len(getFILENAMES(pth_runs))
 TIME      =[]
 for i in range(num_runs):
  filenames=getFILENAMES(os.path.join(pth_runs,'run'+str(i+1)))
  proc     =[int(fn.split('_')[-1].split('.')[0]) for fn in filenames]
  sizeS    =[int(fn.split('_')[ 2]              ) for fn in filenames]
  sizeT    =[int(fn.split('_')[ 4]              ) for fn in filenames]
  time     =[np.loadtxt(fn).item()                for fn in filenames]
  indeces  =np.argsort(proc)
  proc     =[proc [i] for i in indeces]
  sizeS    =[sizeS[i] for i in indeces]
  sizeT    =[sizeT[i] for i in indeces]
  time     =[time [i] for i in indeces]
  TIME.append(time)
 proc      =np.array(proc        )
 sizeS     =np.array(sizeS       )
 sizeT     =np.array(sizeT       )
 TIME      =np.array(TIME        )
 TIME      =np.amin (TIME, axis=0)
 np.savetxt(os.path.join(PATHS[0],'timings'+'.csv' ),
            np.stack((proc,TIME,sizeS,sizeT),axis=1),
            fmt      ='%.4e'                        , 
            delimiter=','                            )
else:
 if PARAMS[0][0] in ['v10','v11']:
  Ua        =[]
  Un        =[]
  N,NT      =[e for e in PARAMS[1]][-1]
  
  filenames=['rank'+str(i)+'.h5' for i in range(len(getFILENAMES(PATHS[2])))]
  
  for i in range(len(filenames)):
   filename =os.path.join(PATHS[2],filenames[i])
   fobj,keys=rHDF(filename)
   Ua.append(fobj['analytic' ][:])
   Un.append(fobj['numerical'][:])
  
  Ua        =np.concatenate(Ua, axis=1)
  Un        =np.concatenate(Un, axis=1)
  
  wHDF(os.path.join(PATHS[0],'data'+'_'+'x'+'_'+str(N)+'_'+'t'+'_'+str(NT)+'.h5'),
       ['numerical','analytic']                                                  ,
       [ Un        , Ua       ]                                                   )


