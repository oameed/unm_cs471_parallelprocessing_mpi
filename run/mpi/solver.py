#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import                                    os
import                                    sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import                time
import                numpy            as np
from mpi4py   import  MPI
from paramd   import  PATHS, PARAMS
from utilsd   import (matvecCHECK                 ,
                      U_analytic                  ,
                      get_L2_norm                 ,
                      wHDF                        ,
                      wCSV                        ,
                      get_local_no_halo_time_stack )
from parld    import (get_proc_halo               ,
                      get_global_dot               )
from geomd    import (get_nodes_bnd               ,
                      get_proc_local_index_domain ,
                      get_proc_local_index_bnd    ,
                      get_proc_local_ngbr         ,
                      get_proc_local_bnd           )
from sparseAd import  get_A_csr
from integd   import (G_analytic                  ,
                      F_analytic                  ,
                      euler_bkwd                   )

COMM                         =  MPI.COMM_WORLD                                                     ### INITIALIZE MPI AND CREATE A COMMUNICATOR
OPS                          = [MPI.SUM       , MPI.MIN, MPI.MAX]
rank                         = COMM.Get_rank()                                                     ### GET YOUR MPI RANK
rank_num                     = COMM.size                                                           ### GET TOTAL NUMBER OF RANKS

error                        =[]                                                                   # TO KEEP ERROR VALUES FOR CONVERGANCE STUDY
time_start_local             =np.array([0.0])    
time_start_global            =np.array([0.0])
time_end_local               =np.array([0.0])
time_end_global              =np.array([0.0])
grid                         =[e for e in PARAMS[1]]                                               ### GET SPATIO-TEMPORAL GRID SIZES

for N,NT in grid:  
 
 config=PARAMS[2][:]
 config.append([COMM,N,OPS[0]])                                                                    ### ADD MPI COMMUNICATOR TO INTEGRATOR'S CONFIGURATION LIST
  
 dt                          =              PARAMS[0][3]/NT                                        
 TIME                        =[(i,i*dt) for i in range(NT+1) if not i==0 ]                         ### GET TEMPORAL POINTS
                                                                                           
 nodes,ngbr,bnd,bndext,dx,Ny =get_nodes_bnd (PARAMS[0][1],                                         ### GET SPATIAL  POINTS AND NEIGHBOR INFORMATION FOR ENTIRE DOMAIN
                                             PARAMS[0][2], 
                                             N            )
 
 halo_start,halo_end         =get_proc_halo              (rank                 ,                   ### GET PROCESSOR HALO REGION INDECES
                                                          rank_num             ,
                                                          Ny                    )
 local_index_domain          =get_proc_local_index_domain([halo_start,halo_end],                   ### GET START AND END INDECES OF  PROCESSOR'S LOCAL NODES
                                                          [N         ,Ny      ] )
 local_index_bnd             =get_proc_local_index_bnd   (bnd                  ,                   ### GET LIST OF       INDECES FOR PROCESSOR'S LOCAL BOUNDARY
                                                          local_index_domain    )
 
 PROCnodes                   =nodes[local_index_domain[0]:local_index_domain[1]+1]
 PROCngbr                    =get_proc_local_ngbr(ngbr,        local_index_domain)
 PROCbnd,PROCbndext          =get_proc_local_bnd (bnd ,bndext, local_index_domain,
                                                               local_index_bnd    )                
                                                                                                   
 
 A                           =get_A_csr     (PROCngbr     )                                        ### GET PROCESSOR'S LOCAL MATRIX A IN CSR FORMAT 
 
 Ua                          =np.zeros((NT+1,A.shape[0]))                                          # SOLUTION: ANALYTIC
 Un                          =np.zeros((NT+1,A.shape[0]))                                          # SOLUTION: NUMERICAL
                                                                                                   
 Ua[0]                       =[U_analytic(  node [0],node[1]           ,0) for node in PROCnodes]  ### SET INITIAL CONDITION
 Un[0]                       = Ua[0]
 
 time_start_local[0]=time.time()
 COMM.Allreduce(time_start_local,time_start_global,op=OPS[1])                                      ### GET THE MINIMUM STARTING TIME
                                                                                           
 for i,T in TIME:                                                                                  ### INTEGRATE FOR NUMBER OF TIME-STEPS
  Ua[i]                      =[U_analytic(  node [0],node[1]           ,T) for node in PROCnodes]
  
  G                          = G_analytic(A.shape[0],PROCbnd,PROCbndext,T)                         ### GET VECTOR OF BOUNDARY VALUES AT EXTERNAL BOUNDARY NODES
  F                          =[F_analytic(  node [0],node[1]           ,T) for node in PROCnodes]  ### GET VECTOR OF FORCING  VALUES AT DOMAIN            NODES
  
  Un[i]                      = euler_bkwd(A*(1/np.power(dx,2)),
                                          Un[i-1]             ,
                                          dt                  ,
                                          G*(1/np.power(dx,2)),
                                          np.array(F)         ,
                                          config               )

 time_end_local[0]=time.time()
 COMM.Allreduce(time_end_local,time_end_global,op=OPS[2])                                          ### GET THE MAXIMUM END      TIME

 if rank==0:
  time_filename                =os.path.join(PATHS[0],'timing'+'_'+'x'+'_'+str(N       )+
                                                               '_'+'t'+'_'+str(NT      )+
                                                               '_'+'p'+'_'+str(rank_num)+
                                                      '.csv'                              )
  wCSV(time_filename,time_end_global-time_start_global)
  print(' FINISHED SIMULATION ({},{}) '.format(N,NT))
 
 if not PARAMS[3][0]:
 
  if N==grid[-1][0]:
   Ua_local_no_halo =get_local_no_halo_time_stack(Ua,COMM,N+1)                                     ###             EXPORT LOCAL  DATA
   Un_local_no_halo =get_local_no_halo_time_stack(Un,COMM,N+1)
   wHDF(os.path.join(PATHS[2],'rank'+str(rank)+'.h5'),
        ['numerical'     ,'analytic'      ],
        [Un_local_no_halo,Ua_local_no_halo] )
 
  error_LOCAL       =Ua[-1]-Un[-1]                                                                 ### COMPUTE AND EXPORT GLOBAL ERROR
  error_GLOBAL_norm2=get_global_dot(error_LOCAL,error_LOCAL,COMM,N+1,OPS[0])
  error_GLOBAL      =np.sqrt(np.power(dx,2)*error_GLOBAL_norm2)
  if rank==0:
   error.append([dx,error_GLOBAL[0]])
  
if not PARAMS[3][0]:

 if rank==0:
  wHDF (os.path.join(PATHS[0],'error'+'.h5'),
        [         'error' ]                 ,
        [ np.array(error) ]                  )


