%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                          %%%
%%% A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 %%%
%%% A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             %%%
%%% GRAPHICS                                                          %%%
%%% by: OAMEED NOAKOASTEEN                                            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TYPE  : '0': TYPICAL SIMULATIONS , ... 
%         '1': STRONG SCALING PLOTS, ...
%         '2': WEAK   SCALING PLOTS
% V     :      SIMULATION VERSION
% CONFIG

function graphics(TYPE,V,CONFIG)
if         TYPE==0
    clc
    PAUSE    =CONFIG{1}                                                                            ;
    filenames=get_filenames(V)                                                                     ;
    error    =rDATA(V, filenames(2),'error'    ,true )                                             ;
    plotter_error(error,...
                  fullfile('..','..','simulations',V,strcat('error','.png')))
    numerical=rDATA(V, filenames(1),'numerical',false)                                             ;
    analytic =rDATA(V, filenames(1),'analytic' ,false)                                             ;
    numerical=reshape(numerical,[sqrt(size(numerical,1)),...
                                 sqrt(size(numerical,1)),...
                                      size(numerical,2)     ])                                     ;
    analytic =reshape(analytic ,[sqrt(size(analytic ,1)),...
                                 sqrt(size(analytic ,1)),...
                                      size(analytic ,2)     ])                                     ;
    csf      =get_color_scale_factor(numerical)                                                    ;
    makeGIF      (numerical,analytic,PAUSE                                  ,...
                  fullfile('..','..','simulations',V,strcat('data' ,'.gif')),...
                  csf                                                           )
else
    if     TYPE==1
        clc
        rFN         =fullfile('..','..','simulations',V{1},strcat('timings'       ,'.csv'))        ;
        wFN         =fullfile('..','..','simulations',V{1},strcat('scaling_strong','.png'))        ;
        timings     =readmatrix(rFN)                                                               ;
        efficiency  =get_efficiency_scaling(timings(:,[1,2]))                                      ;
        plotter_scaling_strong(timings(:,[1,2]),efficiency,2,wFN)
    else
        if TYPE==2            
            clc
            rFN         =fullfile('..','..','simulations',V{1},strcat('timings'     ,'.csv'))      ;
            wFN         =fullfile('..','..','simulations',V{1},strcat('scaling_weak','.png'))      ;
            timings     =readmatrix (rFN)                                                          ;
            efficiency  =get_efficiency_scaling(timings(:,[1,2]))                                  ;
            plotter_scaling_weak(timings,efficiency,1/4,wFN)
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function CSF=get_color_scale_factor(X)
        CSF=max(abs(X),[],"all")                                                                   ;
    end

    function filenames=get_filenames(V)
        function XINDEX=get_index_of_largest_domain(FILENAMES)
            arr           =[]                                                                      ;
            arrind        =[]                                                                      ;
            for i=1:size(FILENAMES,2)
                splits    =split(FILENAMES(i),'_')                                                 ;
                if strcmp(splits(1),'data')
                    arr   =[arr   ,str2num(splits(3))]                                             ;
                    arrind=[arrind,i                 ]                                             ;
                end
            end
        [~,I] =max(arr)                                                                            ;
        XINDEX=arrind(I)                                                                           ;
        end
        function EINDEX=get_index_of_error(FILENAMES)
            arr        =[]                                                                         ;
            for i=1:size(FILENAMES,2)
                splits =split(FILENAMES(i),'.')                                                    ;
                if strcmp(splits(1),'error')
                    arr=[arr,i]                                                                    ;
                end
            end
            EINDEX=arr(1)                                                                          ;
        end
        dirlist  =dir(fullfile('..','..','simulations',V))                                         ;
        filenames=string({dirlist(4:end-1).name})                                                  ;
        Xindex   =get_index_of_largest_domain(filenames)                                           ;
        Eindex   =get_index_of_error         (filenames)                                           ;
        filenames=[filenames(Xindex),filenames(Eindex)]                                            ;
    end

    function DATA=rDATA(SIM,FILENAME,DIR,TRANSPOSE)
        DATA=h5read(fullfile('..','..','simulations',SIM,FILENAME),strcat('/',DIR))                ;
        if TRANSPOSE
            DATA=transpose(DATA)                                                                   ;
        end
    end

    function plotter_error(ERROR,NAME)
        close all
        loglog(ERROR(:,1) ,ERROR(:,1).^2,'--ks',...
               ERROR(:,1) ,ERROR(:,2)   ,'-rs' ,...
               'LineWidth',2                      )
        grid
        xlabel('h'                                )
        legend('Ref. Quadratic','||e||_2')
        saveas(gcf,NAME)
    end
    
    function plotter(NUMERICAL,ANALYTIC,INDEX,CSF)
        subplot(1,2,1)
        surf(NUMERICAL)
        title(['Numerical'])
        xlabel(num2str(INDEX))
        set(gca,'xticklabel',[],'yticklabel',[])
        colormap default
        shading  interp
        axis     equal
        caxis([0 CSF])
        view(2)
        subplot(1,2,2)
        surf(ANALYTIC)
        title(['Analytic'])
        xlabel(num2str(INDEX))
        set(gca,'xticklabel',[],'yticklabel',[])
        colormap default
        shading  interp
        axis     equal
        caxis([0 CSF])
        view(2)
    end

    function makeGIF(NUMERICAL,ANALYTIC,PAUSE,NAME,CSF)
        close all
        h=figure('Position',[680   549   560   560])                                               ;
        for i=1:size(NUMERICAL,3)
            plotter(NUMERICAL(:,:,i),ANALYTIC(:,:,i),i,CSF)
            [imind,cm]=rgb2ind(frame2im(getframe(h)),256)                                          ;
            if i==1
                imwrite(imind,cm,NAME,'gif','Loopcount',inf     ,'DelayTime',PAUSE) 
            else
                imwrite(imind,cm,NAME,'gif','WriteMode','append','DelayTime',PAUSE)
            end
        end
    end

    function E=get_efficiency_scaling(X)
        NUMPROC=X(:,1)                                                                             ;
        TIMING =X(:,2)                                                                             ;
        E      =[]                                                                                 ;
        T1     =NUMPROC(1)*TIMING(1)                                                               ;
        for i=1:size(NUMPROC,1)
            E  =[E;T1/(NUMPROC(i)*TIMING(i))]                                                      ;
        end
    end

    function plotter_scaling_strong(X,E,SPEEDUP,NAME)
        NUMPROC       =X(:,1)                                                                      ;
        TIMING        =X(:,2)                                                                      ;
        timingIDEAL   =zeros(size(TIMING))                                                         ;
        for i=1:size(timingIDEAL,1)
            timingIDEAL(i)=((1/SPEEDUP)^(i-1))*TIMING(1)                                           ;
        end
        close all
        fig=figure                                                                                 ;
        set(fig,'Position',[fig.Position(1),...
                            fig.Position(2),...
                            fig.Position(3),...
                            fig.Position(3)    ])
        sgt           =sgtitle('Strong Scaling')                                                   ;
        sgt.FontSize  =10                                                                          ;
        sgt.FontWeight='bold'                                                                      ;
        offset        =0.15                                                                        ;
        H             =(1-3*offset)/2                                                              ;
        ax1=subplot(2,1,1)                                                                         ;
        semilogx(NUMPROC,E,'--bo','linewidth',1)
        ylim([0,1])
        grid
        ylabel(['Efficiency (relative to ' num2str(NUMPROC(1)) ' cores)'])
        set(gca,'box','off')
        set(ax1,'position',[1.5*offset     ,...
                            offset+H+offset,...
                            1-2.5*offset   ,...
                            H                  ])
        
        ax2=subplot(2,1,2)                                                                         ;
        h(1)=semilogx(NUMPROC,TIMING     ,'--bo','linewidth',1)                                    ;
        hold
        h(2)=semilogx(NUMPROC,timingIDEAL,'--rx','linewidth',1)                                    ;
        ylim([0,floor(1.25*max(TIMING))])
        grid
        ylabel('Run Time (s)' )
        xlabel('Num. of Cores')
        legend(h([2]),'Linear Speedup')
        set(gca,'box','off')
        set(ax2,'position',[1.5*offset    ,...
                            offset        ,...
                            1-2.5*offset  ,...
                            H+offset/1.75     ])
        saveas(gcf,NAME,'png')
    end

    function plotter_scaling_weak(X,E,SPEEDUP,NAME)        
        NUMPROC       =X(:,1)                                                                      ;
        TIMING        =X(:,2)                                                                      ;
        sizeS         =X(:,3)                                                                      ;
        sizeT         =X(:,4)                                                                      ;
        timingIDEAL   =zeros(size(TIMING))                                                         ;
        for i=1:size(timingIDEAL,1)
            timingIDEAL(i)=((1/SPEEDUP)^(i-1))*TIMING(1)                                           ;
        end
        close all
        fig=figure                                                                                 ;
        set(fig,'Position',[fig.Position(1),...
                            fig.Position(2),...
                            fig.Position(3),...
                            fig.Position(3)    ])
        sgt           =sgtitle('Weak Scaling')                                                     ;
        sgt.FontSize  =10                                                                          ;
        sgt.FontWeight='bold'                                                                      ;
        offset        =0.15                                                                        ;
        H             =(1-3*offset)/2                                                              ;
        ax1=subplot(2,1,1)                                                                         ;
        yyaxis left
        semilogx(NUMPROC,E,'--bo','linewidth',1)
        ylabel(['Efficiency (relative to ' num2str(NUMPROC(1)) ' cores)'])
        ylim([0,1])
        yyaxis right
        semilogx(NUMPROC,(sizeS.*sizeT)./1e3,'--ko','linewidth',1)
        ylabel('Spatio-Temporal Size (unit: K)')
        ax1.YAxis(1).Color = 'k'                                                                   ;
        ax1.YAxis(2).Color = 'k'                                                                   ;
        grid
        set(gca,'box','off')
        set(ax1,'position',[1.5*offset     ,...
                            offset+H+offset,...
                            1-2.5*offset   ,...
                            H                  ])
        
        ax2=subplot(2,1,2)                                                                         ;
        h(1)=loglog(NUMPROC,TIMING      ,'--bo','linewidth',1)                                     ;
        hold
        h(2)=loglog(NUMPROC,timingIDEAL ,'--rx','linewidth',1)                                     ;
        ylim([0,floor(1.25*max(TIMING))])
        grid
        ylabel('Run Time (s)' )
        xlabel('Num. of Cores')
        legend(h([2]),[' Factor of ',num2str(1/SPEEDUP),' Timing Increase'])
        set(gca,'box','off')
        set(ax2,'position',[1.5*offset   ,...
                            offset       ,...
                            1-2.5*offset ,...
                            H+offset/1.75    ])
        saveas(gcf,NAME,'png')
    end

end

