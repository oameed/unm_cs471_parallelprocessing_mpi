#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### SERIAL IMPLEMENTATION OF HEAT EQUATION                            ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import                                   os
import                                   sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import              numpy             as np
from paramd   import  PATHS, PARAMS
from utilsd   import (matvecCHECK  ,
                      U_analytic   ,
                      get_L2_norm  ,
                      wHDF          )
from geomd    import  get_nodes_bnd
from sparseAd import  get_A_csr
from integd   import (G_analytic   ,
                      F_analytic   ,
                      euler_bkwd    )

error                        =[]                                                         # TO KEEP ERROR VALUES FOR CONVERGANCE STUDY

for N,NT in PARAMS[1]:
 
 dt                          =              PARAMS[0][3]/NT                                        
 time                        =[(i,i*dt) for i in range(NT+1) if not i==0 ]               ### GET TEMPORAL POINTS
 
                                                                                           
 nodes,ngbr,bnd,bndext,dx,Ny =get_nodes_bnd (PARAMS[0][1],                                         
                                             PARAMS[0][2], 
                                             N            )                              ### GET SPATIAL  POINTS AND NEIGHBOR INFORMATION

 A                           =get_A_csr     (ngbr         )                              ### GET MATRIX A IN CSR FORMAT
 
 Ua                          =np.zeros((NT+1,A.shape[0]))                                # SOLUTION: ANALYTIC
 Un                          =np.zeros((NT+1,A.shape[0]))                                # SOLUTION: NUMERICAL

 Ua[0]                       =[U_analytic(  node [0],node[1]   ,0) for node in nodes]    ### SET INITIAL CONDITION
 Un[0]                       = Ua[0]
                                                                                           
 for i,T in time:                                                                        ### INTEGRATE FOR NUMBER OF TIME-STEPS
  Ua[i]                      =[U_analytic(  node [0],node[1]   ,T) for node in nodes]
  
  G                          = G_analytic(A.shape[0],bnd,bndext,T)                       ### GET VECTOR OF BOUNDARY VALUES AT EXTERNAL BOUNDARY NODES
  F                          =[F_analytic(  node [0],node[1]   ,T) for node in nodes]    ### GET VECTOR OF FORCING  VALUES AT DOMAIN            NODES 

  Un[i]                      = euler_bkwd(A*(1/np.power(dx,2)) ,
                                          Un[i-1]              ,
                                          dt                   ,
                                          G*(1/np.power(dx,2) ),
                                          np.array(F)          ,
                                          PARAMS[2]             )
 
 error.append([dx,get_L2_norm(Ua[-1]-Un[-1],dx)])
  
 wHDF (os.path.join(PATHS[0],'data'+'_'+'x'+'_'+str(N)+'_'+'t'+'_'+str(NT)+'.h5'),
       ['numerical','analytic']                                                  ,
       [ Un        , Ua       ]                                                   )

 print(' FINISHED SIMULATION ({},{}) '.format(N,NT)                               )

wHDF (os.path.join(PATHS[0],'error'+'.h5'),
      [         'error' ]                 ,
      [ np.array(error) ]                  )

print(' FINISHED CONVERGANCE STUDIES '     )


