# A Case Study in Parallel Computing: The Heat Equation

This is a review of the [CS 471 Final Project](doc/A_Case_Study_in_Parallel_Computing.pdf) in which parallelization of _matrix-vector multiplications_, using **_Message Passing Interface (MPI)_**, is studied in the context of the problem of integration, by _implicit Euler method_, of the sinusoidal solutions of the heat equation. A collection of [Computing Cheat Sheets](https://archive.org/details/computing-cheat-sheets) is also included.

## References

_Numerical Analysis_

[ 1 ]. 2018. Heath. _Scientific Computing: An Introductory Survey_. Revised 2nd Edition  
[ 2 ]. 2015. Burden. Faires. _Numerical Analysis_  
[ 3 ]. 2012. Chapra. _Applied numerical methods with MATLAB for engineers and scientists_  
[ 4 ]. 2010. Chapra. Canale. _Numerical methods for engineers_  
[ 5 ]. 2009. Schiesser. _A Compendium of Partial Differential Equation Models: Method of Lines Analysis with Matlab_  
[[ 6 ].](https://web.stanford.edu/class/cme324/) 2000. Saad. _Iterative methods for sparse linear systems_ 1st Edition  
[[ 7 ].](https://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf) 1994. Shewchuk. _An Introduction to the CG Method without the Agonizing Pain_  


_Prallel Computing_

[[ 8 ].](https://github.com/VictorEijkhout/TheArtofHPC_pdfs/tree/main) 2021. Eijkhout. _Introduction to High-Performance Scientific Computing_ 3rd Edition [\[The Art of HPC\]](https://theartofhpc.com/)  
[ 9 ]. 2019. Robey. _Parallel and High-Performance Computing_  
[[10].](https://www.mcs.anl.gov/~itf/dbpp/) 1995. Foster. _Designing and Building Parallel Programs_    

_Online Tutorials and Courses_

[[11].](https://hpc.llnl.gov/training/tutorials/introduction-parallel-computing-tutorial) 0000. LLNL. _Introduction to Parallel Computing_    
[[12].](https://solomonik.cs.illinois.edu/teaching/cs554/index.html) 2017. Solomonik. _Parallel Numerical Algorithms_  


## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.03 s (618.0 files/s, 59433.1 lines/s)
--------------------------------------------------------------------------------
Language                      files          blank        comment           code
--------------------------------------------------------------------------------
Python                            9            119             78            631
MATLAB                            1             15             15            224
Bourne Again Shell                3             92             53            220
Markdown                          1             24              0             68
YAML                              1              1              0             63
Bourne Shell                      2              9              0             23
--------------------------------------------------------------------------------
SUM:                             17            260            146           1229
--------------------------------------------------------------------------------
</pre>
## How to Run
* This project uses [Open MPI](https://www.open-mpi.org/) (version 4.1.5) as an implementation of the _Message Passing Interface (MPI)_ in conjunction with [mpi4py](https://mpi4py.readthedocs.io/en/stable/) (version 3.1.4) as its Python binding. The `YAML` file for creating the conda environment used to run this project is included in `run/conda`.
* To run convergance studies on Desktop machine:
  1. `cd` to the main project directory
  2. `.\run\ps1\run_v10.ps1`
* To run convergance studies on HPC system:
  1. `cd` to `run/slrm`
  2. `sbatch run_v11.sh`
* To run strong scaling studies on HPC system:
  1. `cd` to `run/slrm`
  2. `sbatch run_v2x.sh`
* To run weak scaling studies on HPC system:
  1. `cd` to `run/slrm`
  2. `sbatch run_v3x.sh`

## Simulations 'v2x' and 'v3x': Scaling Studies

|                  |                |
|:---:             |:---:           |
|**Strong Scaling**|**Weak Scaling**|
|<img src="simulations/v20/scaling_strong.png"  width=785/> | <img src="simulations/v30/scaling_weak.png" width=785/> |

## Simulation 'v11': 8 Processors on HPC System

|            |                         |
|:---:       |:---:                    |
|**Solution**|**Convergance Studies**  |
|<img src="simulations/v11/data.gif"  width=929/> | <img src="simulations/v11/error.png" width=640/> |


## Simulation 'v10': 8 Processors on Desktop Machine

|            |                         |
|:---:       |:---:                    |
|**Solution**|**Convergance Studies**  |
|<img src="simulations/v10/data.gif"  width=929/> | <img src="simulations/v10/error.png" width=640/> |

## Simulation 'v01': Serial Implementation

|            |                         |
|:---:       |:---:                    |
|**Solution**|**Convergance Studies**  |
|<img src="simulations/v01/data.gif"  width=929/> | <img src="simulations/v01/error.png" width=640/> |




