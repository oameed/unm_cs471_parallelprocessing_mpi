#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### INTEGRATION FUNCTION DEFINITIONS                                  ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import                    numpy      as np
from scipy.sparse import  eye        as speye
from scipy.sparse import  diags      as spdiags
from utilsd       import  U_analytic
from parld        import (communicate   ,
                          get_global_dot )

def G_analytic(SHAPE,INDEX,BNDEXT,T):
 gv=np.zeros((SHAPE))
 for i in range(SHAPE):
  if i in       INDEX :
   for node in  BNDEXT[INDEX.index(i)]:
    gv[i]=gv[i]+U_analytic(node[0],node[1],T)
 return gv

def F_analytic(X,Y,T):
 return np.pi*np.cos(np.pi*X)*np.cos(np.pi*Y)*(np.cos(np.pi*T)+(2*np.pi)*np.sin(np.pi*T))

def JACOBI(A,B,X0,CONFIG):
 def get_residual_norm(a,b,x):
  r  =np.ravel(b-a*x)
  return np.sqrt(np.dot(r,r))
 I   =speye  (A.shape[0]    , format='csr')
 Dinv=spdiags(1/A.diagonal(), format='csr')
 x   =X0
 rn0 =get_residual_norm(A,B,X0)
 for i in range(CONFIG[1]):
  x  = (I-Dinv*A)*x+Dinv*B
  rn =get_residual_norm(A,B,x)
  if (rn/rn0 < CONFIG[0]):
   break
 if i  <  CONFIG[1]-1:
  print (' JACOBI CONVERGED WITH '+str(i+1)+' ITERATIONS')
 else:
  if i == CONFIG[1]-1:
   print(' WARNING: JACOBI DID NOT CONVERGE! '           )
 return x

def JACOBI_PARL(A,B,X0,CONFIG):
 return

def CG(A,B,X0,CONFIG):
 def get_residual(a,b,x):
  return np.ravel(b-a*x)
 x=X0
 r=get_residual(A,B,X0)
 v=r
 for i in range(CONFIG[1]):
  r_norm2_prev = np.dot(r,r)
  t            = r_norm2_prev/np.dot(v,A*v)
  x            = x+t*v
  r            = r-t*(A*v)
  r_norm2_curr = np.dot(r,r)
  s            = r_norm2_curr/r_norm2_prev
  v            = r+s*v
  if (np.sqrt(r_norm2_curr) < CONFIG[0]):
   break
 if i  <  CONFIG[1]-1:
  print (' CG CONVERGED WITH '+str(i+1)+' ITERATIONS')
 else:
  if i == CONFIG[1]-1:
   print(' WARNING: CG DID NOT CONVERGE! '           )
 return x

def CG_PARL(A,B,X0,CONFIG):
 def get_local_residual(a,b,x):
  return np.ravel(b-a*x)
 COMM    =CONFIG[2][0]
 N       =CONFIG[2][1]
 OP      =CONFIG[2][2]
 rank    =COMM.Get_rank()
 rank_num=COMM.size
 x       =X0
 x       =communicate(x,COMM,N+1)
 r       =get_local_residual(A,B,X0)
 r       =communicate(r,COMM,N+1)
 v       =r
 v       =communicate(v,COMM,N+1)
 for i in range(CONFIG[1]):
  r_norm2_prev=             get_global_dot(r,r  ,COMM,N+1,OP)
  t           =r_norm2_prev/get_global_dot(v,A*v,COMM,N+1,OP)
  x           = x+t*v
  x           =communicate(x,COMM,N+1)
  r           = r-t*(A*v)
  r           =communicate(r,COMM,N+1)
  r_norm2_curr=             get_global_dot(r,r  ,COMM,N+1,OP)
  s           = r_norm2_curr/r_norm2_prev
  v           = r+s*v
  v           =communicate(v,COMM,N+1)
  if (np.sqrt(r_norm2_curr) < CONFIG[0]):
   break
 if rank==0:
  if i  <  CONFIG[1]-1:
   print (' CG CONVERGED WITH '+str(i+1)+' ITERATIONS')
  else:
   if i == CONFIG[1]-1:
    print(' WARNING: CG DID NOT CONVERGE! '           )
 return x

def euler_bkwd(AMATRIX,U,DT,G,F,CONFIG):
 A     =speye(AMATRIX.shape[0],format='csr')-DT*AMATRIX
 b     =U+DT*G+DT*F
 x0    =U
 
 if len(CONFIG)==3:                         # SERIAL SOLVERS 
  if CONFIG[0]:
   U_next =JACOBI     (A,b,x0,CONFIG[1:])   
  else:
   U_next =CG         (A,b,x0,CONFIG[1:])
 else:
  if len(CONFIG)==4:                        # PARALLEL SOLVERS 
   if CONFIG[0]:
    U_next=JACOBI_PARL(A,b,x0,CONFIG[1:])
   else:
    U_next=CG_PARL    (A,b,x0,CONFIG[1:])
 return U_next


