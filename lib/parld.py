#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### PARALLEL PROCESSING FUNCTION DEFINITIONS                          ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import numpy as np

def get_proc_halo(RANK,RANKNUM,NY):
 indeces=np.array_split(np.array([i for i in range(NY+1)]),RANKNUM)[RANK] ### ALOCATE DOMAIN ROWS    TO  THE PROCESSOR
 if RANKNUM==1:                                                           ### DETERMINE HALO REGIONS FOR THE PROCESSOR
  halo_start=indeces[ 0]
  halo_end  =indeces[-1]
 else:
  if RANK  ==0:
   halo_start=indeces[ 0]
   halo_end  =indeces[-1]+1
  else:
   if RANK ==RANKNUM-1:
    halo_start=indeces[ 0]-1
    halo_end  =indeces[-1]
   else:
    halo_start=indeces[ 0]-1
    halo_end  =indeces[-1]+1
 return halo_start, halo_end

def communicate(XLOCAL,COMM,N):
 rank     =COMM.Get_rank()
 rank_num =COMM.size
 var_start=np.zeros((N))                                                  # RECEIVED FROM PREVIOUS RANK
 var_end  =np.zeros((N))                                                  # RECEIVED FROM NEXT     RANK
 if rank != rank_num-1 :                                                  ### SEND    TO   THE NEXT     RANK
  COMM.Send(XLOCAL[-2*N: -N], dest  =rank+1, tag=77)
 if rank != 0          :                                                  ### RECEIVE FROM THE PREVIOUS RANK
  COMM.Recv(var_start       , source=rank-1, tag=77)
 if rank != 0          :                                                  ### SEND    TO   THE PREVIOUS RANK
  COMM.Send(XLOCAL[   N:2*N], dest  =rank-1, tag=77)
 if rank != rank_num-1 :                                                  ### RECEIVE FROM THE NEXT     RANK
  COMM.Recv(var_end         , source=rank+1, tag=77)
 if rank_num != 1      :                                                  ### PLACE COMMUNICATED START/END REGIONS IN THEIR RESPECTIVE PLACE
  if rank==0           :
   XLOCAL [-N: ]=var_end
  else:
   if rank==rank_num-1 :
    XLOCAL[  :N]=var_start
   else:
    XLOCAL[-N: ]=var_end
    XLOCAL[  :N]=var_start
 return XLOCAL

def get_local_no_halo(XLOCAL,COMM,N):
 rank    =COMM.Get_rank()
 rank_num=COMM.size
 if rank_num==1       :
  X  =XLOCAL
 else:
  if rank==0          :
   X =XLOCAL[ :-N]
  else:
   if rank==rank_num-1:
    X=XLOCAL[N:  ]
   else:
    X=XLOCAL[N:-N]
 return X

def get_global_sum(XLOCAL,COMM,N,OP):
 XGLOBAL=np.array([0.0])
 COMM.Allreduce(XLOCAL,XGLOBAL,op=OP)
 return XGLOBAL

def get_global_dot(XLOCAL,YLOCAL,COMM,N,OP):
 X   =get_local_no_halo(XLOCAL,COMM,N   )
 Y   =get_local_no_halo(YLOCAL,COMM,N   )
 PROD=np.dot(X,Y)
 PROD=get_global_sum   (PROD  ,COMM,N,OP)
 return PROD


