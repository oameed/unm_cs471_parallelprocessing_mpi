#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### UTILITIES FUNCTION DEFINITIONS                                    ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import                                  os
import             numpy             as np
import             h5py
from parld import get_local_no_halo

def matvecCHECK(A,ROWS,COLS):
 # USAGE FOR SERIAL  : matvecCHECK(A,N+1                    ,N+1)
 # USAGE FOR PARALLEL: matvecCHECK(A,(halo_end-halo_start)+1,N+1)
 SHAPE =A.shape[0]
 vector=A*np.ones((SHAPE))
 vector=np.reshape(vector,[ROWS,COLS])
 print(vector)

def U_analytic(X,Y,T):
 return np.cos(np.pi*X)*np.cos(np.pi*Y)*np.sin(np.pi*T)

def get_L2_norm(E,DX):
 return np.sqrt(np.power(DX,2)*np.dot(E,E))

def get_local_no_halo_time_stack(XLOCAL,COMM,N):
 time_stack=[]
 for i in range(XLOCAL.shape[0]):
  time_stack.append(get_local_no_halo(XLOCAL[i],COMM,N))
 return np.stack(time_stack,axis=0)

def wHDF(FILENAME,DIRNAMES,DATA):
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def getFILENAMES(PATH,COMPLETE=True):
 filename=[]
 for FILE in os.listdir(PATH):
  if not FILE.startswith('.'):
   if COMPLETE:
    filename.append(os.path.join(PATH,FILE)) 
   else:
    filename.append(FILE)
 return filename

def rHDF(FILENAME):
 fobj=h5py.File(FILENAME,'r')
 keys=[key for key in fobj.keys()]
 return fobj,keys

def wCSV(FILENAME,DATA):
 np.savetxt(FILENAME, DATA, fmt='%.4e', delimiter=',')


