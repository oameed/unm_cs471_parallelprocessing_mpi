#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### GEOMETRIC DOMAIN FUNCTION DEFINITIONS                             ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import numpy as np

def get_nodes_bnd(XEND,YEND,N):
 precision=10

 def get_1d_boundary_index(J,I,X,Y,NODES,LIST):
  BND=[]
  for  j in J:
   for i in I:
    BND.append([X[i],Y[j]])
  LIST.append([NODES.index(i) for i in BND])

 def get_0d_boundary_index(COORD,NODES,LIST):
  LIST.append([NODES.index(COORD)         ])

 delta    =    (XEND-0)/N
 Ny       =int((YEND-0)/delta)
 x        =[round(i*delta,precision) for i in range(N +1)]                                           
 y        =[round(i*delta,precision) for i in range(Ny+1)]                                           
 nodes    =[]                                                                        #   LIST OF ALL NODES OF THE GRID
 bound    =[]                                                                        #   LIST OF ALL NODES OF THE GRID THAT ARE ON THE BOUNDARIES 
 neighbors=[]                                                                        #   LIST OF INTERIOR NEIGHBORS       FOR BOUNDARY NODES
 NEIGHBORS=[]                                                                        #   LIST OF          NEIGHBORS       FOR INTERIOR NODES
 ngbr     =[]                                                                        #   LIST OF          NEIGHBORS       FOR ALL GRID POINTS
 bndngbr  =[]                                                                        #   LIST OF EXTERIOR NEIGHBOR COORDS FOR BOUNDARY NODES
 for  j in range(len(y)):                                                            ### GET ALL NODES
  for i in range(len(x)):
   nodes.append([x[i],y[j]])
 get_1d_boundary_index([0       ]       ,range(1,len(x)-1),x,y,nodes,bound)          ### GET NODES ON 1D BOUNDARY
 get_1d_boundary_index(range(1,len(y)-1),[len(x)-1]       ,x,y,nodes,bound)
 get_1d_boundary_index([len(y)-1]       ,range(1,len(x)-1),x,y,nodes,bound)
 get_1d_boundary_index(range(1,len(y)-1),[0       ]       ,x,y,nodes,bound)
 get_0d_boundary_index([0    ,0    ]                          ,nodes,bound)          ### GET NODES ON 0D BOUNDARY
 get_0d_boundary_index([x[-1],0    ]                          ,nodes,bound)
 get_0d_boundary_index([x[-1],y[-1]]                          ,nodes,bound)
 get_0d_boundary_index([0    ,y[-1]]                          ,nodes,bound) 
 for i in range(len(bound[0])):                                                      ### GET INTERIOR NEIGHBORS FOR 1D BOUNDARY NODES 
  node              = nodes[bound[0][i]]
  node_to_the_right =[round(node[0]+delta,precision),      node[1]                 ]
  node_to_the_top   =[      node[0]                 ,round(node[1]+delta,precision)]
  node_to_the_left  =[round(node[0]-delta,precision),      node[1]                 ]
  neighbors.append(  [nodes.index(node_to_the_right),
                      nodes.index(node_to_the_top  ),
                      nodes.index(node_to_the_left ) ])
  node_to_the_bottom=[      node[0]                 ,round(node[1]-delta,precision)] ### GET EXTERIOR NEIGHBOR COORDS FOR 1D BOUNDARY NODES
  bndngbr.append(    [node_to_the_bottom         ]    )

 for i in range(len(bound[1])): 
  node              = nodes[bound[1][i]]
  node_to_the_bottom=[      node[0]                 ,round(node[1]-delta,precision)]
  node_to_the_top   =[      node[0]                 ,round(node[1]+delta,precision)]
  node_to_the_left  =[round(node[0]-delta,precision),      node[1]                 ]
  neighbors.append(  [nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_top   ),
                      nodes.index(node_to_the_left  ) ])
  node_to_the_right =[round(node[0]+delta,precision),      node[1]                 ]
  bndngbr.append(    [node_to_the_right          ]     )

 for i in range(len(bound[2])): 
  node              = nodes[bound[2][i]]
  node_to_the_bottom=[      node[0]                 ,round(node[1]-delta,precision)]
  node_to_the_right =[round(node[0]+delta,precision),      node[1]                 ]
  node_to_the_left  =[round(node[0]-delta,precision),      node[1]                 ]
  neighbors.append(  [nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_right ),
                      nodes.index(node_to_the_left  ) ])
  node_to_the_top   =[      node[0]                 ,round(node[1]+delta,precision)]
  bndngbr.append(    [node_to_the_top            ]     )
 
 for i in range(len(bound[3])): 
  node              = nodes[bound[3][i]]
  node_to_the_bottom=[      node[0]                 ,round(node[1]-delta,precision)]
  node_to_the_right =[round(node[0]+delta,precision),      node[1]                 ]
  node_to_the_top   =[      node[0]                 ,round(node[1]+delta,precision)]
  neighbors.append([  nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_right ),
                      nodes.index(node_to_the_top   ) ])
  node_to_the_left  =[round(node[0]-delta,precision),      node[1]                 ]
  bndngbr.append(    [node_to_the_left           ]     )
 
 node               = nodes[bound[4][0]]                                             ### GET INTERIOR NEIGHBORS FOR 0D BOUNDARY NODES 
 node_to_the_right  =[round(node[0]+delta,precision),      node[1]                 ]
 node_to_the_top    =[      node[0]                 ,round(node[1]+delta,precision)]
 neighbors.append(   [nodes.index(node_to_the_right ),
                      nodes.index(node_to_the_top   ) ])
 node_to_the_left   =[round(node[0]-delta,precision),      node[1]                 ] ### GET EXTERIOR NEIGHBOR COORDS FOR 0D BOUNDARY NODES
 node_to_the_bottom =[      node[0]                 ,round(node[1]-delta,precision)]
 bndngbr.append(     [node_to_the_left  ,
                      node_to_the_bottom         ]     )
 
 node               = nodes[bound[5][0]]            
 node_to_the_top    =[      node[0]                 ,round(node[1]+delta,precision)]
 node_to_the_left   =[round(node[0]-delta,precision),      node[1]                 ]
 neighbors.append(   [nodes.index(node_to_the_top   ),
                      nodes.index(node_to_the_left  ) ])
 node_to_the_right  =[round(node[0]+delta,precision),      node[1]                 ]
 node_to_the_bottom =[      node[0]                 ,round(node[1]-delta,precision)]
 bndngbr.append(     [node_to_the_right ,
                      node_to_the_bottom         ]     )
 
 node               = nodes[bound[6][0]]                
 node_to_the_bottom =[      node[0]                 ,round(node[1]-delta,precision)]
 node_to_the_left   =[round(node[0]-delta,precision),      node[1]                 ]
 neighbors.append(   [nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_left  ) ])
 node_to_the_right  =[round(node[0]+delta,precision),      node[1]                 ]
 node_to_the_top    =[      node[0]                 ,round(node[1]+delta,precision)]
 bndngbr.append(     [node_to_the_right,
                      node_to_the_top            ]     )
 
 node               = nodes[bound[7][0]]            
 node_to_the_bottom =[      node[0]                 ,round(node[1]-delta,precision)]
 node_to_the_right  =[round(node[0]+delta,precision),      node[1]                 ]
 neighbors.append(   [nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_right ) ])
 node_to_the_top    =[      node[0]                 ,round(node[1]+delta,precision)]
 node_to_the_left   =[round(node[0]-delta,precision),      node[1]                 ]
 bndngbr.append(     [node_to_the_top ,
                      node_to_the_left           ]     )
 
 bound      =bound[0]+bound[1]+bound[2]+bound[3]+bound[4]+bound[5]+bound[6]+bound[7] ### SORT BOUNDARY POINTS ACCORDING TO THEIR NUMBER
 indexsorted=sorted(range(len(bound)), key=lambda k: bound[k])
 bound      =[bound    [i] for i in indexsorted]
 neighbors  =[neighbors[i] for i in indexsorted]                                     ### SORT INTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 bndngbr    =[bndngbr  [i] for i in indexsorted]                                     ### SORT EXTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 indexinside=[i for i in range(len(nodes)) if i not in bound]                        ### GET  NEIGHBORS FOR INTERIOR NODES
 for i in range(len(indexinside)):                 
  node              = nodes[indexinside[i]]
  node_to_the_bottom=[round(node[0]-delta,precision),      node[1]                 ]
  node_to_the_right =[      node[0]                 ,round(node[1]+delta,precision)]
  node_to_the_top   =[round(node[0]+delta,precision),      node[1]                 ]
  node_to_the_left  =[      node[0]                 ,round(node[1]-delta,precision)]
  NEIGHBORS.append(  [nodes.index(node_to_the_bottom),
                      nodes.index(node_to_the_right ),
                      nodes.index(node_to_the_top   ),
                      nodes.index(node_to_the_left  ) ])                    
 for  i in range(len(nodes)):                                                        ### GENERATE AN ORDERED LIST OF NEIGHBORS FOR ALL GRID POINTS
  if  i in bound      :
   INDEX = bound.index(i)
   LIST  = neighbors
  else                :
   if i in indexinside:
    INDEX= indexinside.index(i)
    LIST = NEIGHBORS  
  ngbr.append(LIST[INDEX])
 return nodes, ngbr, bound, bndngbr, delta, Ny

def get_proc_local_index_domain(HALO,N):
 element_index_start= HALO[0]   *(N[0]+1)
 element_index_end  =(HALO[1]+1)*(N[0]+1)-1
 return [element_index_start,element_index_end]

def get_proc_local_index_bnd(BND,INDEXLOC):
 arr=[i for i   in range     (INDEXLOC[1]+1) if np.greater_equal(i,INDEXLOC[0])]
 idx=[i for i,e in enumerate (BND          ) if e in arr                       ]
 return idx

def get_proc_local_ngbr(NGBR,ELEMENTS):
 ngbrPROC           =[]
 ngbr               =NGBR[ELEMENTS[0]:ELEMENTS[1]+1]                                ### SELECT BLOCK OF NODES (AND THE HALO REGION) THAT BELONG TO THE PROCESSOR
 for node in ngbr:
  ngbrPROC.append([index-ELEMENTS[0] for index in node])                            ### RE-NUMBER THE BLOCK NODES
 for  node    in ngbrPROC:                                                          ### REMOVE THE NODES OUTSIDE OF THE PROCESSOR REGION
  for element in node    :
   if np.logical_or(element<0,element>(ELEMENTS[1]-ELEMENTS[0])):
    node.remove(element)
 return ngbrPROC

def get_proc_local_bnd(BND,BNDEXT,ELEMENTS,BNDINDEX):
 bnd   =[BND   [i] for i in BNDINDEX]
 bndext=[BNDEXT[i] for i in BNDINDEX]
 offset=bnd[0]
 bnd   =[i-offset  for i in bnd     ]
 return bnd,bndext


