#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### PARAMETER DEFINITIONS                                             ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-v'     , type=str  ,               help='SIMULATION VERSION'           , required=True        )
parser.add_argument('-x'     , type=float, default=1   , help='X AXIS END POINT'                                    )
parser.add_argument('-y'     , type=float, default=1   , help='Y AXIS END POINT'                                    )
parser.add_argument('-t'     , type=float, default=0.5 , help='Y AXIS END POINT'                                    )
parser.add_argument('-c'     , type=int  , default=0   , help='CONFIGURATION SELECTOR'                              )
parser.add_argument('-jacobi',                           help='JACOBI SOLVER SELECTOR'       ,action  ='store_true' )
parser.add_argument('-tol'   , type=float, default=1e-6, help='TOLERANCE FOR SOLVER'                                )
parser.add_argument('-mitr'  , type=int  , default=500 , help='MAXIMUM ITERATIONS FOR SOLVER'                       )
parser.add_argument('-noexp' ,                           help='DONT EXPORT DATA OR ERROR'    ,action  ='store_true' )

### ENABLE FLAGS
args  = parser.parse_args()

## CONSTRUCT PARAMETER STRUCTURES

PATHS =[ os.path.join('..','..','simulations',args.v       ),
         os.path.join('..','..','simulations',args.v,'logs'),
         os.path.join('..','..','simulations',args.v,'data') ]

PARAMS=[[args.v, args.x, args.y, args.t]]

if       args.c==0:                                                  # FOR DEBUGGING
 PARAMS.append      (zip([8  ],
                         [8  ]                             ))
else:
 if      args.c==1:                                                  # FOR CONVERGANCE STUDIES
  PARAMS.append     (zip([8, 8*(2**1), 8*(2**2), 8*(2**3)],
                         [8, 8*(2**2), 8*(2**4), 8*(2**6)] ))
 else:
  if     args.c==2:                                                  # FOR STRONG SCALING;       T=0.015 (dt/dx**2 aprox. 3.8)
   PARAMS.append    (zip([256],
                         [256]                             ))
  else:
   if    args.c==3:                                                  # FOR WEAK   SCALING (Np=1 ,T=0.030)
    PARAMS.append   (zip([16  ],
                         [1*48]                            ))
   else:
    if   args.c==4:                                                  # FOR WEAK   SCALING (Np=4 ,T=0.030) ### zip([70 ],[2*48]) 
     PARAMS.append  (zip([64  ],
                         [2*48]                            ))
    else:
     if  args.c==5:                                                  # FOR WEAK   SCALING (Np=10,T=0.030) ### zip([160],[3*48]) 
      PARAMS.append (zip([128 ],
                         [3*48]                            ))
     else:
      if args.c==6:                                                  # FOR WEAK   SCALING (Np=16,T=0.030) ### zip([290],[4*48]) 
       PARAMS.append(zip([256 ],
                         [4*48]                            ))
    

PARAMS.append([args.jacobi,args.tol,args.mitr])
PARAMS.append([args.noexp                    ])

# PARAMS[0][0]: SIMULATION VERSION
# PARAMS[0][1]: X    AXIS END POINT
# PARAMS[0][2]: Y    AXIS END POINT
# PARAMS[0][3]: TIME AXIS END POINT
# PARAMS[1]   : ITERATOR FOR TUPLES OF (SPATIAL,TEMPORAL) DIVISIONS
# PARAMS[2][0]: JACOBI SOLVER SELECTOR
# PARAMS[2][1]: TOLERANCE FOR SOLVER
# PARAMS[2][2]: MAXIMUM ITERATION FOR SOLVER
# PARAMS[2][3]: MPI COMMUNICATOR
# PARAMS[3][0]: DATA/ERROR EXPORT CONTROL


