#########################################################################
### UNIVERSITY OF NEW MEXICO                                          ###
### A REVIEW OF CS 471 INTRODUCTION TO SCIENTIFIC COMPUTING FALL 2019 ###
### A CASE STUDY IN PARALLEL COMPUTING: THE HEAT EQUATION             ###
### SPARSE MATRIX CONSTRUCTION FUNCTION DEFINITIONS                   ###
### by: OAMEED NOAKOASTEEN                                            ###
#########################################################################

from scipy.sparse import csr_matrix

def get_A_csr(X): 
 DATA   =[]                                                                         # 'data'    FOR STANDARD CSR REPRESENTATION 
 indeces=[]                                                                         # 'indeces' FOR STANDARD CSR REPRESENTATION 
 indptr =[]                                                                         # 'indptr'  FOR STANDARD CSR REPRESENTATION 
 data   =[]
 x      =[e[:] for e in X]
 for  i in range(len(x)):                                                           ### TO GET ALL NON-ZERO ELEMENTS OF EACH ROW, APPEND SELF INDEX
  x[i].append(i)
 for  i in range(len(x   )  ):                                                      ### GET 'data' (PER EACH NODE)
  data.append([])
  for j in range(len(x[i])-1):
   data[i].append( 1)
  data [i].append(-4)
 for  i in range(len(x)):                                                           ### SORT INDEX OF NEIGHBORS ACCORDING TO NUMBER
  element    =x[i]
  indexsorted=sorted(range(len(element)), key=lambda k:element[k])
  x   [i]    =[x   [i][j] for j in indexsorted]
  data[i]    =[data[i][j] for j in indexsorted]                                     ### SORT 'data' (PER EACH NODE) ACCORDINGLY
 for  i in range(len(x)):                                                           ### GT 'data', 'indeces' AND 'indptr'
  position   =len(indeces)
  indeces    =indeces+ x   [i]
  indptr     =indptr +[position]
  DATA       =DATA   + data[i]  
 indptr.append(len(indeces)) 
 return csr_matrix((DATA,indeces,indptr),shape=(len(x),len(x)))


